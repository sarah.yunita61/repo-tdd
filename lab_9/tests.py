from django.http import HttpRequest
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .api_enterkomputer import get_drones
from .views import index, response as response_index, my_cookie_auth, is_login
from .csui_helper import get_access_token, get_data_user, get_client_id, verify_user
from .api_enterkomputer import get_drones

import environ
import requests

root = environ.Path(__file__) - 3  # three folder back (/a/b/c/ - 3 = /)
env = environ.Env(DEBUG=(bool, False), )
environ.Env.read_env('.env')


# Create your tests here.
class Lab9UnitTest(TestCase):
    def setUp(self):
        self.username = env("SSO_USERNAME")
        self.password = env("SSO_PASSWORD")

    # views test
    def test_lab_9_url_is_exist(self):
        response = Client().get('/lab-9/')
        self.assertEqual(response.status_code, 200)

    def test_lab_9_using_index_func(self):
        found = resolve('/lab-9/')
        self.assertEqual(found.func, index)

    def test_lab_9_index_condition(self):
        response = self.client.get('/lab-9/')
        self.assertEqual(response.status_code, 200)

        # logged in, redirect to profile page
        self.assertNotIn("user_login", self.client.session)
        html_content = response.content.decode('utf-8')
        self.assertIn("Halaman Login", html_content)

        response = self.client.post('/lab-9/custom_auth/login/', {'username': self.username, 'password': self.password})
        self.assertEqual(response.status_code, 302)

        login = self.client.get('/lab-9/')
        self.assertEqual(login.status_code, 302)

    def test_profile(self):
        # Not logged in
        response = self.client.get('/lab-9/profile/')
        self.assertEqual(response.status_code, 302)

        # Logged in
        self.client.post('/lab-9/custom_auth/login/', {'username': self.username, 'password': self.password})
        response = self.client.get('/lab-9/profile/')
        self.assertEqual(response.status_code, 200)

    def test_set_data(self):  # baru kondisi else
        self.client.session['drones'] = response_index['drones']
        response = self.client.post('/lab-9/custom_auth/login/')
        self.assertEqual(response_index['fav_drones'], [])

    def test_add_delete_and_reset_favorite_drones(self):
        response = self.client.post('/lab-9/custom_auth/login/', {'username': self.username, 'password': self.password})
        self.assertEqual(response.status_code, 302)

        #add drone
        response = self.client.post('/lab-9/add_session_drones/'+get_drones().json()[0]["id"]+'/')
        response = self.client.post('/lab-9/add_session_drones/'+get_drones().json()[1]["id"]+'/')
        html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
        self.assertEqual(response.status_code, 302)
        self.assertIn("Berhasil tambah drone favorite", html_response)

        #delete drone
        response = self.client.post('/lab-9/del_session_drones/'+get_drones().json()[0]["id"]+'/')
        html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
        self.assertEqual(response.status_code, 302)
        self.assertIn("Berhasil hapus dari favorite", html_response)

        #reset drones
        response = self.client.post('/lab-9/clear_session_drones/')
        html_response = self.client.get('/lab-9/profile/').content.decode('utf8')
        self.assertEqual(response.status_code, 302)
        self.assertIn("Berhasil reset favorite drones", html_response)


    def test_cookie_login(self):
        cookie_not_login = self.client.post('/lab-9/cookie/login/')
        self.assertEqual(cookie_not_login.status_code, 200)
        self.assertIn('Login menggunakan COOKIES', cookie_not_login.content.decode('utf8'))

        cookie_login = self.client.post('/lab-9/cookie/auth_login/', {'username': 'utest', 'password': 'ptest'})
        cookie_is_login = self.client.post('/lab-9/cookie/login/')
        self.assertEqual(cookie_is_login.status_code, 302)

    def test_cookie_auth_login(self):
        method_get = self.client.get('/lab-9/cookie/auth_login/')
        self.assertEqual(method_get.status_code, 302)

        wrong_input = self.client.post('/lab-9/cookie/auth_login/', {'username': 'aku', 'password': 'cantik'})
        self.assertEqual(wrong_input.status_code, 302)
        self.assertRaisesMessage("Username atau Password Salah", wrong_input)

        correct_input = self.client.post('/lab-9/cookie/auth_login/', {'username': 'utest', 'password': 'ptest'})
        self.assertEqual(correct_input.status_code, 302)

    def test_cookie_profile(self):
        not_logged_in = self.client.get('/lab-9/cookie/profile/')
        self.assertEqual(not_logged_in.status_code, 302)

        self.client.cookies.load({'user_login': 'u', 'user_password': 'p'})
        logged_in_wrong_input = self.client.get('/lab-9/cookie/profile/')
        self.assertRaisesMessage(logged_in_wrong_input, "Kamu tidak punya akses :P ")
        self.assertEqual(logged_in_wrong_input.status_code, 200)
        self.client.post('/lab-9/cookie/auth_login/',
                         {'username': 'utest', 'password': 'ptest'})

        correct_input = self.client.get('/lab-9/cookie/profile/')
        self.assertEqual(correct_input.status_code, 200)
        self.assertIn("[Cookie] Profile", correct_input.content.decode('utf8'))

    def test_cookie_clear(self):
        cookie_clear = self.client.get('/lab-9/cookie/clear/')
        self.assertEqual(cookie_clear.status_code, 302)
        self.assertRaisesMessage(cookie_clear, "Anda berhasil logout. Cookies direset")

    def test_my_cookie_auth(self):
        result_true = my_cookie_auth('utest', 'ptest')
        self.assertTrue(result_true)
        result_false = my_cookie_auth('aku', 'cantik')
        self.assertFalse(result_false)

    def test_is_login(self):
        request = HttpRequest()
        test_false = is_login(request)
        self.assertFalse(test_false)

    # custom_auth test
    def test_auth_login(self):
        login_1 = self.client.post('/lab-9/custom_auth/login')
        self.assertEqual(login_1.status_code, 301)
        self.assertRaisesMessage(login_1, "Username atau password salah")
        login_1 = self.client.get('/lab-9/custom_auth/login')
        self.assertRaisesMessage(login_1, "Username atau password salah")

        login_2 = self.client.post('/lab-9/custom_auth/login/', {'username': self.username, 'password': self.password})
        self.assertRaisesMessage("Anda berhasil login", login_2)
        self.assertEqual(login_2.status_code, 302)

    def test_auth_logout(self):
        login = self.client.post('/lab-9/custom_auth/login/', {"username": self.username, "password": self.password})
        logout = self.client.get('/lab-9/custom_auth/logout/')
        self.assertEqual(logout.status_code, 302)
        self.assertRaisesMessage("Anda berhasil logout. Semua session Anda sudah dihapus", logout)

    # csui_helper test
    def test_get_access_token(self):
        username = "aku cantik"
        password = "oke"

        test_1 = get_access_token(username, password)
        self.assertTrue(type(test_1), str)
        self.assertIsNone(test_1)
        self.assertRaises(Exception, get_access_token(username, password))

        test_2 = get_access_token(self.username, self.password)
        self.assertIsNotNone(test_2)

    def test_get_client_id(self):
        client_id = get_client_id()
        self.assertTrue(type(client_id), str)
        self.assertEqual(client_id, "X3zNkFmepkdA47ASNMDZRX3Z9gqSU1Lwywu5WepG")

    def test_verify_id(self):
        verified = verify_user("1234567890")
        self.assertIn("error_description", verified)

    def test_get_data_user(self):
        access_token = '123456789'
        id = 'test'
        data_user = get_data_user(access_token, id)
        self.assertIn("detail", data_user)
        self.assertEqual(data_user["detail"], "Authentication credentials were not provided.")

    # test api enterkomputer
    def test_get_drones(self):
        response = get_drones()
        self.assertEqual('<Response [200]>', str(response))
        self.assertEqual(type(response), requests.Response)
